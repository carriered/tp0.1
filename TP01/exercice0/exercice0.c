//standard input output et lib
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//pour open
#include <fcntl.h>

//pour read close and write
#include <unistd.h>

int trier(const void *ptr1, const void *ptr2);

int main(int argc, char* argv[]){
    if(argc != 3){
        printf("mauvais nombre d'argument\n");
        exit(1);
    }
    int fd_in = open(argv[1],O_RDONLY);
    int fd_out = open(argv[1],O_WRONLY | O_CREAT | O_TRUNC, 0777);

    if(fd_in < 0 || fd_out < 0){
        perror(argv[1]);
        perror(argv[2]);
        exit(2);
    }

    char* buffer = malloc(1 * sizeof(char));
    char* allBuffer = malloc(4096 * sizeof(char)); //taille max des nombres: 1024
    int *tab = malloc(8192 * sizeof(int)); //nombre maximum de nombres en tout: 2048
    int nbread;
    int nbint = 0;

    do {
        nbread = read(fd_in,buffer,1);

        //si buffer contient un espace ou un saut de ligne
        if(buffer[0] == ' ' || buffer[0] == '\n'){

            *(tab+nbint) = atoi(allBuffer);
            nbint++;
            allBuffer[0] = '\0'; //pour vider la chaine de caractères
        } else {
            strcat(allBuffer,buffer);
        }
    } while (nbread == 1);

    free(buffer);
    free(allBuffer);
    tab = realloc(tab,nbint * sizeof(int));
    close(fd_in);

    qsort( tab, nbint, sizeof(int), trier);

    char *bufferInt = malloc(8192 * sizeof(int));
    char *waitInt = malloc(4096 * sizeof(char));
    for(int i = 0; i < nbint; i++){
        sprintf(waitInt,"%i",tab[i]);
        strcat(bufferInt,waitInt);
        strcat(bufferInt, " ");
    }

    write(fd_out,bufferInt, strlen(bufferInt) * sizeof(char));

    close(fd_out);
    return 0;
}

int trier(const void *ptr1, const void *ptr2){
    int first_int = *(const int*) ptr1;
    int second_int = *(const int*) ptr2;
    return first_int - second_int;
}
