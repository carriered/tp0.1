#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char* argv[]){
    if(argc < 2){
        printf("Mauvais nombre d'arguments.\n");
        exit(1);
    }

    int fd = open(argv[1],O_RDWR | O_CREAT, 0777);
    if(fd < 0){
        perror("Le fichier n'a pas réussi à s'ouvrir");
        exit(2);
    }

    for(int i = 2; i < argc; i++ ){
        if (strlen(argv[i]) == 1){
            printf("erreur sur la taille de l'argument %i: %s\n",i,argv[i]);
            exit(3);
        }
        if(argv[i][0] == 's'){
            long decalage = strtol(&argv[i][1],NULL,10);
            lseek(fd, decalage, SEEK_SET);
            printf("%s : déplacement du curseur de %li\n",argv[i],decalage);
        }
        else if(argv[i][0] == 'w'){
            char *phrase = &argv[i][1];
            write(fd, phrase, strlen(phrase));
            printf("%s : écriture de %lu bits\n",argv[i] ,strlen(phrase));
        }
        else if(argv[i][0] == 'r'){
            long longueur = strtol(&argv[i][1],NULL,10);
            char *buffer = malloc(sizeof(char) * longueur);
            read(fd, buffer, longueur);
            printf("%s : %s\n", argv[i], buffer);
            free(buffer);
        } else {
            printf("erreur sur le nom de l'argument %i: %s\n",i,argv[i]);
            exit(4);
        }
    }

    close(fd);
    return 0;
}