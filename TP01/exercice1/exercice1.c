#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

int main(int argc, char* argv[]){
    if(argc == 1 || argc > 3){
        printf("mauvais nombre d'arguments\n");
        exit(1);
    }



    char *buffer = malloc(4096 * sizeof(char));
    int nbRead;
    int nbWrite;
    int fd_out;



    if(argc == 2){
        fd_out = open(argv[1], O_CREAT | O_WRONLY | O_TRUNC);
    } else if(strcmp(argv[2],"-a") == 0) {
        fd_out = open(argv[1], O_CREAT | O_WRONLY | O_APPEND);
    } else {
        printf("mauvaise option\n");
        exit(2);
    }


    if(fd_out < 0){
        printf("erreur sur le fichier\n");
        perror(argv[1]);
        exit(3);
    }

    nbRead = read(fileno(stdin),buffer,4096);

    nbWrite = write(fileno(stdout),buffer,nbRead);
    if(nbRead != nbWrite){
        printf("tout les octets n'ont pas été écrits dans la sortie standard /!\\");
    }
    nbWrite = write(fd_out,buffer,nbRead);
    if(nbRead != nbWrite){
        printf("tout les octets n'ont pas été écrits dans le fichier spécifié en argument /!\\");
    }

    free(buffer);
    close(fd_out);


    return 0;
}