#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

pid_t getPPid(pid_t pid);

int main(int argc, char* argv[]){
    pid_t pid = getpid();
    pid_t pid_precedent = 0;

    while(pid != pid_precedent){
        pid_precedent = pid;
        if(pid != 0){
            pid = getPPid(pid);
        }
    }
    printf("la génération de base est: %i\n",pid);
    exit(0);
}

pid_t getPPid(pid_t pid){
    char cmd[80];
    sprintf(cmd,"ps -o ppid= -p %d", (int) pid);
    FILE *f;
    char res[10];
    f = popen(cmd,"r");
    fgets(res,10,f);
    pclose(f);
    return atoi(res);
}