#include <stdio.h>
#include <stdlib.h>

pid_t getPPid();

int main(int argc,char* argv[]){
    if(argc != 2 ){
        printf("il n'y a un erreur\n");
        exit(1);
    }
    pid_t pid = atoi(argv[1]);
    if(pid <= 0){
        printf("une autre erreur\n");
        exit(2);
    }
    pid_t ppid = getPPid(pid);

    printf("le ppid du processus %i est %i\n",pid, ppid);
    exit(0);

}

pid_t getPPid(pid_t pid){
    char cmd[80];
    sprintf(cmd,"ps -o ppid= -p %d", (int) pid);
    FILE *f;
    char res[10];
    f = popen(cmd,"r");
    fgets(res,10,f);
    pclose(f);
    return atoi(res);
}